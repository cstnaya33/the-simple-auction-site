<?php 
	session_start();	
	include('ConnectToMySQL.php');

	function text_input($data){
		$data = trim($data);
		$data = stripslashes($data);
		$data = htmlspecialchars($data);
		return $data;
	}
	$user = text_input($_POST['user']);
	$pword = text_input($_POST['pword']);		

	$stmt = $db->prepare("SELECT * FROM UserAccount WHERE account=?");
	$stmt->bind_param("s", $user);
	$stmt->execute();
	
	$result= $stmt->get_result();
	$person = $result->fetch_row();

	//$result = mysqli_query($db,$sql);
	//$person = mysqli_fetch_row($result);
	
	$DB_userPw = $person[2];
	$hashpword = password_hash($pword, PASSWORD_DEFAULT);

	if (password_verify($pword, $DB_userPw)){
		$_SESSION['USER'] = $person[0];
		$_SESSION['RIGHT'] = $person[5];

		echo '<meta http-equiv=REFRESH CONTENT=1;url=home.php>';	
	}

	else{
		echo '<script>alert("Login Failure");</script>';
		echo '<meta http-equiv=REFRESH CONTENT=1;url=index.html>';
	}


?>