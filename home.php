<?php 
	session_start();
	include('ConnectToMySQL.php');

	if (!isset($_SESSION['USER'])) 
	{
		//echo '<meta http-equiv=REFRESH CONTENT=1;url=index.html>';
		echo '<script>window.location.href = "index.html"</script>';
    	exit;
   	} 

   	function text_input($data){
		$data = trim($data);
		$data = stripslashes($data);
		$data = htmlspecialchars($data);
		return $data;
	}

   	$user=$_SESSION['USER'];
   	$right=text_input($_SESSION['RIGHT']);

   	$search=0;
   	if(isset($_POST['search'])){
   		$search=$_POST['search'];
   	}

	$line=" House.id "; $sorting=" ASC ";
   	if (isset($_GET['sort']) && isset($_GET['line'])){
		$sorting=text_input($_GET['sort']);
		$line=text_input($_GET['line']);
	}
	if (isset($_POST['sort']) && isset($_POST['line'])){
		$sorting=text_input($_POST['sort']);
		$line=text_input($_POST['line']);
	}

	$re=$adv_info=" ";
	if (!empty($_POST['re']) && isset($_POST['re'])){
		//echo '<script>alert("$re ")</script>';
		$re=urldecode($_POST['re']);
	}
	if (!empty($_POST['adv_info']) && isset($_POST['adv_info'])){
		$adv_info=urldecode($_POST['adv_info']);
	}

	$name=$location=$owner="";

	if (!empty($_POST['name'])){
		$name=urldecode($_POST['name']);
	}
	if (!empty($_POST['location'])){
		$location=urldecode($_POST['location']);
	}
	if (!empty($_POST['owner'])){
		$owner=urldecode($_POST['owner']);
	}

	$count=0;
	if (isset($_POST['count'])){
		$count=$_POST['count'];
	}
	
	$orders=""; $ordert="";

	if (!empty($_POST['orders']) && isset($_POST['orders'])){
		//echo '<script>alert("$orders")</script>';
		$orders=new DateTime(text_input($_POST['orders']));
		$orders=$orders->format('Y-m-d');
	}

	if (!empty($_POST['ordert'])&& isset($_POST['ordert'])){
		//echo '<script>alert("$ordert")</script>';
		$ordert=new DateTime(text_input($_POST['ordert'])); 
		$ordert=$ordert->format('Y-m-d');
	}
	$pages="";$data_nums="";

?>

<html>
<head>
	<meta charset="UTF-8">
	<title>home</title>
	<link rel="stylesheet" href="list_bar.css">
	<link rel="stylesheet" href="tri_style.css">
	
	<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
</head>
<body>
	<p>
		<tr >
		<td align="center" valign="center"><a href="house.php">HouseArrangement </a></td><br>
		<td align="center" valign="center"><a href="fav.php">Favorite </a></td><br>
		<td align="center" valign="center"><a href="housepage.php">housepage </a></td><br>
		<td align="center" valign="center"><a href="orderpage.php">orderpage </a></td><br>
		<td align="center" valign="center"><a href="logout.php">Log Out </a></td><br>
			<?php if($right){
				?><td align="center" valign="center"><a href="homeAdmin.php">Member Arrangement</a></td><br>
				<td align="center" valign="center"><a href="table.php">Table Arrange </a></td><br><?php
			} ?>
		</tr>
	</p>

	<table style="width:100%">
  	<tr>
  		<th>ID</th>
  		<th>name</th>
    	<th>
    		<table style = 'width: 100%; text-align: center;'><tr><td>
					<form method="post" action="home.php?line=price&sort=ASC" class="inline">
						<input type="hidden" name="page" value="1">
						<input type="hidden" name="re" value="<?php echo urlencode($re) ;?>">
						<input type="hidden" name="search" value="<?php echo $search; ?>">
		

						<input type="hidden" name="ordert" value="<?php echo $ordert; ?>">
						<input type="hidden" name="orders" value="<?php echo $orders; ?>">

					
						<input type="hidden" name="adv_info" value="<?php echo urlencode($adv_info) ;?>">
						<button type="submit" name="submit" value="sort" class="link-button">
							<span class='tri_up'></span>
						</button>
					</form>
				</td>
				<td>price</td>
				<td>
					<form method="post" action="home.php?line=price&sort=DESC" class="inline">
						<input type="hidden" name="page" value="1">
						<input type="hidden" name="re" value="<?php echo urlencode($re) ;?>">
						<input type="hidden" name="ordert" value="<?php echo $ordert; ?>">
						<input type="hidden" name="orders" value="<?php echo $orders; ?>">
						<input type="hidden" name="search" value="<?php echo $search; ?>">
						<input type="hidden" name="adv_info" value="<?php echo urlencode($adv_info) ;?>">
						<button type="submit" name="submit" value="sort" class="link-button">
							<span class='tri_down'></span>
						</button>
					</form>
				</td></tr></table>
    	</th>
    	<th>location</th> 
    	<th>
    		<table style = 'width: 100%; text-align: center;'><tr><td>
					<form method="post" action="home.php?line=time&amp;sort=ASC" class="inline">
						<input type="hidden" name="re" value="<?php echo urlencode($re);?>">
						<input type="hidden" name="ordert" value="<?php echo $ordert; ?>">
						<input type="hidden" name="orders" value="<?php echo $orders; ?>">
						<input type="hidden" name="search" value="<?php echo $search; ?>">
						
						<input type="hidden" name="adv_info" value="<?php echo urlencode($adv_info) ;?>">
						<button type="submit" name="submit" value="sort" class="link-button">
							<span class='tri_up'></span>
						</button>
					</form>
				</td>
				<td>time</td>
				<td>
					<form method="post" action="home.php?line=time&amp;sort=DESC" class="inline">
						<input type="hidden" name="re" value="<?php echo urlencode($re) ;?>">
						<input type="hidden" name="ordert" value="<?php echo $ordert; ?>">
						<input type="hidden" name="orders" value="<?php echo $orders; ?>">
						<input type="hidden" name="search" value="<?php echo $search; ?>">
						
						<input type="hidden" name="adv_info" value="<?php echo urlencode($adv_info) ;?>">
						<button type="submit" name="submit" value="sort" class="link-button">
							<span class='tri_down'></span>
						</button>
					</form>
				</td></tr></table>
    	</th>
    	<th>owner</th>
    	<th>information</th>
    	<th>option</th>
    	<?php if($right){
				?><th>option</th><?php
			} ?>
		<th>option</th>
  	</tr>

  	<a href="search.php">Advance search</a><br>
  	<!-- <br><input type="button" value="Sign in" onclick="location.href='search.php'"></input><br> -->
  	

	<?php
	

		if (!isset($_POST["page"])){ //假如$_GET["page"]未設置
		        $page=1; //則在此設定起始頁數
		    } else {
		        $page = intval($_POST["page"]); //確認頁數只能夠是數值資料
		}
		$per = 5; //每頁顯示項目數量
	    $startpage = ($page-1)*$per; //每一頁開始的資料序號
	    
	    if ($search){
	    	echo $user."<br>";

		$str="SELECT * From House Inner join UserAccount On House.owner_id=UserAccount.id 
				inner join house_location on  house_location.house_id=House.id
				inner join Location on Location.id=house_location.id $re $adv_info ORDER BY $line $sorting";
		
		$sql=$db->prepare($str);
		echo "<br>".$str."<br>";
		$val="";
		if (!empty($_POST['name']) || !empty($_POST['location']) || !empty($_POST['owner'])){
			echo $count."<br>";
			switch($count){
				case 1:	$sql->bind_param("sss",$name, $ordert, $orders);	break;
				case 4:	$sql->bind_param("sss",$location, $ordert, $orders);	break;
				case 9:	$sql->bind_param("sss",$owner, $ordert, $orders);	break;
				case 5:	$sql->bind_param("ssss",$name, $location, $ordert, $orders);	break;
				case 10: $sql->bind_param("ssss",$name, $owner, $ordert, $orders);	break;	
				case 13: $sql->bind_param("ssss",$location, $owner, $ordert, $orders);	break;
				case 14: $sql->bind_param("sssss",$name, $location, $owner, $ordert, $orders);	break;
			}		
		}
		else{
			if (!empty($ordert) && !empty($orders) ){
				echo $ordert."<br>".$orders;
				$sql->bind_param("ss", $ordert, $orders);	
			}
			
		}

		//$sql->bind_param("s",$name);
		$sql->execute();
		$result= $sql->get_result();

		
	    $data_nums = mysqli_num_rows($result); //統計總比數
	    $pages = ceil($data_nums/$per); //取得不小於值的下一個整數
	    
	    $str="SELECT * From House Inner join UserAccount On House.owner_id=UserAccount.id 
				inner join house_location on  house_location.house_id=House.id
				inner join Location on Location.id=house_location.id $re $adv_info ORDER BY $line $sorting";
		

	    $str=$str.' LIMIT '.$startpage.', '.$per ;
	    echo "<br>".$startpage."<br>".$per."<br>";

	    $sql=$db->prepare($str);
		if (!empty($_POST['name']) || !empty($_POST['location']) || !empty($_POST['owner'])){
			echo $count."<br>";
			switch($count){
				case 1:	$sql->bind_param("sss",$name, $ordert, $orders);	break;
				case 4:	$sql->bind_param("sss",$location, $ordert, $orders);	break;
				case 9:	$sql->bind_param("sss",$owner, $ordert, $orders);	break;
				case 5:	$sql->bind_param("ssss",$name, $location, $ordert, $orders);	break;
				case 10: $sql->bind_param("ssss",$name, $owner, $ordert, $orders);	break;	
				case 13: $sql->bind_param("ssss",$location, $owner, $ordert, $orders);	break;
				case 14: $sql->bind_param("sssss",$name, $location, $owner, $ordert, $orders);	break;
			}		
		}
		else{
			if (!empty($ordert) && !empty($orders) ){
				$sql->bind_param("ss", $ordert, $orders);	
			}	
		}
		//$sql->bind_param("s",$name);
		$sql->execute();
		$result= $sql->get_result();
		 
		//$result=mysqli_query($db,$sql);

		$account=0;
		$id=0; $name=1; $price=2;  $time=3; $owner_name=6;

    	while($row=$result->fetch_row()){ 
    		if ($account==$row[$id]){
    			continue;
    		}
    		$account=$row[$id];
	?>

	<tr>
		<td align="center" valign="center"><?php echo $row[$id]; $dd=$row[$id];?></td>	<!--id-->
		<td align="center" valign="center"><?php echo $row[$name]; ?></td>	<!--name-->
    	<td align="center" valign="center"><?php echo $row[$price]; ?></td>	<!--price-->
    	<td align="center" valign="center"><?php 

    		$loca_sql="SELECT name FROM `house_location` inner join Location on house_location.id=Location.id Where house_id='$row[$id]'";
			$loca_result=$db->query($loca_sql);
			$loca_row=mysqli_fetch_row($loca_result);
			if(mysqli_num_rows($loca_result) == 0){
				echo "unknown";
			}
			else{
				echo $loca_row[$id]."<br>"."<br>";
			}


    	 ?></td>	<!--locate-->
	    <td align="center" valign="center"><?php echo $row[$time] ?></td>	<!--time-->
	    <td align="center" valign="center"><?php echo $row[$owner_name] ?></td>	<!--owner-->
		<td align="center" valign="center"><?php 

			$info_sql="SELECT Information.name FROM Information 
						INNER JOIN house_Information On house_Information.id=Information.id 
						Where house_id='$row[$id]'";

			$info_result=$db->query($info_sql);

			while($info_row=mysqli_fetch_row($info_result)){
				echo $info_row[$id]."<br>";	
				
			}
			echo "<br>";

		?></td>
		<td align="center" valign="center">
			<a href="home.php?ref=<?php echo $row[$id]; ?>">Favorite</a>
		</td>
		<td align="center" valign="center"><?php if($right){	?>
			<a href="home.php?del=<?php echo $row[$id]; ?>">Delete</a><?php
			} ?>
			
		</td>

		<td align="center" valign="center">
			<form method="POST">
			<input type="hidden" name="orders" value="<?php echo $orders;?>">
			<input type="hidden" name="ordert" value="<?php echo $ordert;?>">

			<input type="hidden" name="book" value="<?php echo $row[$id];?>">
			<input type="submit"></form>
  	</tr>


	<?php
		}
	}
    
	    //分頁頁碼
		 
	    echo '共 '.$data_nums.' 筆-在 '.$page.' 頁-共 '.$pages.' 頁';
	    echo "<br /><a href=?page=1> 首頁 </a> ";
	    echo "第  ";
	    for( $i=1 ; $i<=$pages ; $i++ ) {
	          //echo " <a href=?page=".$i.  ">". $i ." </a> ";
	    	?>
	    	<form method="post">
				<input type="hidden" name="re" value="<?php echo urlencode($re) ;?>">
				<input type="hidden" name="page" value="<?php echo $i ;?>">
				<input type="hidden" name="line" value="<?php echo $line ;?>">
				<input type="hidden" name="sort" value="<?php echo $sorting ;?>">
				<input type="hidden" name="ordert" value="<?php echo $ordert; ?>">
				<input type="hidden" name="orders" value="<?php echo $orders; ?>">
				<input type="hidden" name="search" value="<?php echo $search; ?>">
						
				<input type="hidden" name="adv_info" value="<?php echo urlencode($adv_info) ;?>">
				<input type="submit" name="pagesub" value="<?php echo $i ;?>">
			</form> <?
	    } 
	    echo "  頁 <a href=?page=".$pages. "> 末頁 </a><br /><br />";
	

    $ref="";
    if(isset($_GET['ref'])){
		$ref=text_input($_GET['ref']);
	}
	if(!empty($ref)){	
		$fav_sql="SELECT * from Favorite WHERE user_id='$user' AND favorite_id='$ref'"; 
		$fav_result=mysqli_query($db,$fav_sql); 
		$fav_row=mysqli_fetch_row($fav_result);
		
		if(!$fav_row){
			$addfav_sql= $db->prepare("INSERT INTO Favorite(user_id, favorite_id) VALUES ('$user',?)");
			$addfav_sql->bind_param("s", $ref);
			$addfav_sql->execute();
			//$addfav_result=mysqli_query($db,$addfav_sql);
			if ($addfav_sql){
				echo '<script>alert("Add to Favorite list Succeeded")</script>';	
			}
		}
		else{
			echo '<script>alert("Add to Favorite list Failed. You may have added it.")</script>';
		}
		echo '<script>window.location.href = "home.php"</script>';
	}

	$del="";
	if(isset($_GET['del'])){
		$del=text_input($_GET['del']);
	}
	if (!empty($del)){
		$del_stmt = $db->prepare("DELETE FROM `House` WHERE id=?");
			$del_stmt->bind_param("s", $del);
			$del_stmt->execute();

		if($del_stmt){
			echo '<script>alert("Delete Succeeded")</script>';
	    }
		else{
			echo '<script>alert("Delete member Failed.")</script>';
		}
		echo '<script>window.location.href = "home.php"</script>';	
	}
	
	$book=""; //id for the house
	$start=""; // id for the first day
	$days=""; //booking interval
	$order="";

	if(isset($_POST['book'])){
		$book=text_input($_POST['book']);
	}
	if (!empty($book)){
			/*$day=new DateInterval('P'.$days.'D'); // P開頭代表日期，10D 代表 10 天
			$start=new DateTime(text_input($_POST['start']));
			$tmp  =new DateTime(text_input($_POST['start']));
			$end=$tmp->add($day); 
			$end=$end->format('Y-m-d');
			$start=$start->format('Y-m-d');

			echo $start."<br>".$end."<br>";
			*//*
			select * from `Order` 
					where house_id in (select house_id from `Order` where begin>?  or end<=?) 
					and house_id=$book and user_id=$user
			*/
			$start=$orders; $end=$ordert;
			echo $orders."<br>".$ordert."<br>".$user."<br>";

			$test=$db->prepare("SELECT * FROM `Order` where house_id in 
							(select house_id from `Order` where end<='$ordert' and end>'$orders' 	 and house_id=$book and user_id=$user) or 
				house_id in (select house_id from `Order` where begin<'$ordert' and begin>='$orders' and house_id=$book and user_id=$user) or 
				house_id in (select house_id from `Order` where begin<='$orders' and end>='$ordert'  and house_id=$book and user_id=$user)");
			//$test->bind_param("ssssss", $ordert, $orders, $ordert, $orders, $orders, $ordert);
			$test->execute();
			$test=$test->get_result();

			if (mysqli_num_rows($test) == 0){					
				$order=$db->prepare("INSERT INTO `Order`(`house_id`, `user_id`, `begin`, `end`) VALUES (?, $user, '$orders', '$ordert')");
				$order->bind_param("i", $book);
				$order->execute();
			}

			if($order){
				echo '<script>alert("Order Succeeded")</script>';
	    	}
			else{
				echo '<script>alert("Failed. Maybe this house has been booked in this interval")</script>';
			}
		
	}

	?>

	</table> 
</body>
</html>
