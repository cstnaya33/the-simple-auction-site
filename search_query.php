<?php
	session_start();
	include('ConnectToMySQL.php');

	function text_input($data){
		$data = trim($data);
		$data = stripslashes($data);
		$data = htmlspecialchars($data);
		return $data;
	}

	
	/*************************************************/

	$flag=1;
	$search=1;
	$id=$price1=$price2=1;
	$start=" time>='1895-01-11' ";	$end=" time<='2200-11-11' ";
	$orders; $ordert;

	if (!empty($_POST['orders'])){
		$orders=$_POST['orders']; 
	}

	if (!empty($_POST['ordert'])){
		$ordert=$_POST['ordert']; 
	}

	$order="";

	if (isset($orders) && isset($ordert)){
		if ($ordert<$orders){
			echo '<script>alert("Wrong input of order date.")</script>';	
		}
		$order=" House.id in (select House.id from `House` where House.id not in (select house_id from `Order` where begin<=? and end>=? )) ";
	}

	//echo $order;

	if (!empty($_POST['start'])){
		$start=" time>='".text_input($_POST['start'])."' "; 
	}

	if (!empty($_POST['end'])){
		$end=" time<='".text_input($_POST['end'])."' ";
	}

	if (!empty($_POST['id'])){
		if (!is_numeric($_POST['id'])){
			$flag=0;
			echo '<script>alert("Wrong price interval input format");</script>';
			echo '<meta http-equiv=REFRESH CONTENT=1;url=search.php>';
		}
		$id=' House.id='.text_input($_POST['id'])." ";
	}

	$tmp=0; $tmp2=(1<<20);
	if (!empty($_POST['price1'])){
		if (!is_numeric($_POST['price1'])){
			$flag=0;
			echo '<script>alert("Wrong price interval input format");</script>';
			echo '<meta http-equiv=REFRESH CONTENT=1;url=search.php>';
		}
		$tmp=$_POST['price1'];
	}

	if (!empty($_POST['price2'])){
		if (!is_numeric($_POST['price2'])){
			$flag=0;
			echo '<script>alert("Wrong price interval input format");</script>';
			echo '<meta http-equiv=REFRESH CONTENT=1;url=search.php>';
		}
		$tmp2=$_POST['price2'];
	}

	if ((!empty($_POST['price1'])) && (!empty($_POST['price2'])) && ($tmp != min($tmp, $tmp2))){
		$tmp+=$tmp2;
		$tmp2=$tmp-$tmp2;
		$tmp-=$tmp2;
	}

	if ($tmp!=0){
		$price1 = ' price>='.text_input($tmp);
	}

	if ($tmp2!=(1<<20)){
		$price2 = ' price<='.text_input($tmp2);
	}

/*	if (!empty($_POST['location'])){
		$location = 'location=\''.text_input($_POST['location']).'\'';
	}
	if (!empty($_POST['name'])){
		$name = ' House.name=\''.text_input($_POST['name']).'\' ';
	}
	if (!empty($_POST['owner'])){
		$owner = ' UserAccount.name like %'.text_input($_POST['owner']).'\'%) ';
	}
*/
	
	$location=$owner=$name=1;
	$name_val=$location_val=$owner_val="";
	$str2=$s_num="";
	$count=0;
	$search=$_POST['s'];
	if(!empty($_POST['s'])){
		foreach($search as $k=>$v){
			if ($k=='name' && !empty($v)){ //name
				$name="House.name=?";
				$name_val=$v;
				$s_num.="s";
				$str2.= ",$".$k;
				$count+=1;
			}
			if ($k=='location' && !empty($v)){ //location
				$location="Location.name=?";
				$location_val=$v;
				$s_num.="s";
				$str2.= ",$".$k;
				$count+=4;
			}
			if ($k=='owner' && !empty($v)){ //owner
				$owner="UserAccount.name like ?";
				$owner_val="%".$v."%";
				$s_num.="s";
				$str2.= ",$".$k;
				$count+=9;
			}
		}
	}

	$re=" left  join `Order` on House.id=Order.house_id WHERE $id  AND $price1 AND $price2 AND $name AND $location AND $owner AND $start AND $end AND $order ";

	/*****************************************************/

	$adv_info="";
	if (!empty($_POST['info'])){
		$info_checkbox = $_POST['info'];
		foreach ($_POST['info'] as $info){
    		$adv_info.=" and House.id in 
    					(select house_Information.house_id from Information 
    						inner join house_Information on house_Information.id=Information.id 
    						where Information.name='$info') ";
    	}
    	//$adv_info=substr_replace($adv_info,' ',-4,4); 
    }	
?> <html><body>
	<form method="POST" id="myform" action="home.php">
  		<input type="hidden" name="count" value="<?php echo urlencode($count); ?>">
  		<input type="hidden" name="name" id="0" value="<?php echo urlencode($name_val); ?>">
  		<input type="hidden" name="location" id="1" value="<?php echo urlencode($location_val); ?>">
  		<input type="hidden" name="owner" id="2" value="<?php echo urlencode($owner_val); ?>">
  		<input type="hidden" name="ordert" value="<?php echo $ordert; ?>">
		<input type="hidden" name="orders" value="<?php echo $orders; ?>">
		<input type="hidden" name="search" value="<?php echo $search; ?>">
  		<input type="hidden" name="re" value="<?php echo urlencode($re); ?>">
  		<input type="hidden" name="adv_info" value="<?php echo urlencode($adv_info); ?>">
  		<input type="submit">
	</form></body></html>

<?php		
//	echo '<script>window.location.href="home.php?re='.$re.'&adv_info='.$adv_info.'"</script>';
	if ($flag){
		
		echo '<script>window.setTimeout(function() { document.forms["myform"].submit(); }, 0);</script>';	
	}	
?>
