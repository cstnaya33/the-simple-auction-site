<?php 
	session_start();
	include('ConnectToMySQL.php');

	function text_input($data){
		$data = trim($data);
		$data = stripslashes($data);
		$data = htmlspecialchars($data);
		return $data;
	}
?>
<html>
<head>
	<meta charset="UTF-8">
	<title>House Management</title>
</head>
<body>
	<tr >
		<td align="center" valign="center"><a href="home.php">Home</a></td><br>
		<td align="center" valign="center"><a href="logout.php">Log out</a></td><br>
	</tr>
	<br><br>
		<tr>
		<td align="center" valign="center"><a href="new.php">New</a></td><br>
		<td align="center" valign="center"><a href="update.php">Update</a></td><br><br>
		</tr>
<?php 
	$user=$_SESSION['USER'];	

		$sql=$db->prepare("SELECT * From House 
			  Inner join UserAccount On House.owner_id=UserAccount.id where owner_id=?");
 		$sql->bind_param("s",$user);
		$sql->execute();
		$result= $sql->get_result();
		
		if (mysqli_num_rows($result) == 0) { 
   			echo "You don't have any house.<br>"; 
		} 
		else { 
			?>
			<table style="width:100%">
	 	 	<tr>
	  		<th>ID</th>
	  		<th>name</th>
	  		<th>price</th>
	  		<th>location</th>
	  		<th>Time</th>
	    	<th>information</th>
	    	<th>option</th>
	    	</tr>
	    	<?php 

			$id=0; $name=1; $price=2; $location=3; $time=3; $owner_name=8;
			while($row=$result->fetch_row()){ 
	?>

			<tr>
			<td align="center" valign="center"><?php echo $row[$id] ?></td>	<!--id-->
			<td align="center" valign="center"><?php echo $row[$name] ?></td>	<!--name-->
	    	<td align="center" valign="center"><?php echo $row[$price] ?></td>	<!--price-->
	    	<td align="center" valign="center"><?php 

    		$loca_sql="SELECT name FROM `house_location` inner join Location on house_location.id=Location.id Where house_id='$row[$id]'";
			$loca_result=$db->query($loca_sql);
			$loca_row=mysqli_fetch_row($loca_result);
			if(mysqli_num_rows($loca_result) == 0){
				echo "unknown";
			}
			else{
				echo $loca_row[$id]."<br>"."<br>";
			}
			
    	 ?></td>	<!--locate-->
		    <td align="center" valign="center"><?php echo $row[$time] ?></td>	<!--time-->
			<td align="center" valign="center"><?php 

				$info_sql="SELECT Information.name FROM Information 
						INNER JOIN house_Information On house_Information.id=Information.id 
						Where house_id='$row[$id]'";

				$info_result=$db->query($info_sql);

				while($info_row=mysqli_fetch_row($info_result)){
					echo $info_row[$id]."<br>";
				}
				echo "<br>";

			?> </td>
			<td align="center" valign="center"><a href="house.php?del=<?php echo $row[$id]; ?>">Delete</a></td>
  			</tr>
		<?php 
		} 

		$del="";
		if(isset($_GET['del'])){
			$del=text_input($_GET['del']);
		}

		if (!empty($del)){
			$del_stmt = $db->prepare("DELETE FROM `House` WHERE id=?");
				$del_stmt->bind_param("s", $del);
				$del_stmt->execute();

			if($del_stmt){
				echo '<script>alert("Delete Succeeded")</script>';
		    }
			else{
				echo '<script>alert("Delete Failed.")</script>';
			}
			echo '<script>window.location.href = "house.php"</script>';	
		}

	}
		 ?>
</body>
</html>