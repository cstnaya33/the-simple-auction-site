<?php 
	session_start();
	include('ConnectToMySQL.php');

	if (!isset($_SESSION['USER'])) {
		//echo '<meta http-equiv=REFRESH CONTENT=1;url=index.html>';
		echo '<script>window.location.href = "index.html"</script>';
    	exit;
   	} 

?>
<html>
<head>
	<meta charset='UTF-8'>
	<title>home</title>
</head>
<body>
	<?php
		$me=$_SESSION['USER'];
		$sql="SELECT * FROM `UserAccount` WHERE id=$me";

		$result = mysqli_query($db,$sql);
		$person = mysqli_fetch_row($result);
	?>

	<h2>Hi, <?php echo $person[3];?> </h2>
	<p>
		Account: <?php echo $person[1];?>
	</p>
	<p>
		Email: <?php echo $person[4];?>
	</p>

	<h2>=========================</h2>

	<a href="home.php">Back to home page</a><br>

	<a href="signin.php" target="_blank">new account</a>
	<a href="logout.php">logout</a>

	<table style="width:100%">
  	<tr>
  		<th>Delete</th>
  		<th>LevelUp</th>
    	<th>ID</th>
    	<th>Account</th> 
    	<th>Name</th>
    	<th>Email</th>
    	<th>User/Admin</th>
  	</tr>


	<?php
		
	/*	if (!function_exists('ForDelete')) {
    		function ForDelete($id){
    			
    		}
    	}
    	
		if (!function_exists('ForLevelUP')) {
    		function ForLevelUP($id){
    			$LevelUPSQL = "UPDATE `UserAccount` SET isAdmin=1 WHERE account='$id'";
    			mysqli_query($db,$LevelUPSQL);
    			if ($LevelUPSQL){
    				echo "<script>alert('Succeeded')</script>";
    			}
    		}
    	} */


	 	while($user = mysqli_fetch_row($user_result)){
       
    ?>   

	<tr>
		<td align="center" valign="center">
			<a href="homeAdmin.php?del=<?php echo $user[1]; ?>">Delete</a>
		</td>

		<td align="center" valign="center">
			<a href="homeAdmin.php?lev=<?php echo $user[1]; ?>">Level Up</a>
		</td>

    	<td align="center" valign="center"><?php echo $user[0]; ?></td>
    	<td align="center" valign="center"><?php echo $user[1]; ?></td>
	    <td align="center" valign="center"><?php echo $user[3]; ?></td>
	    <td align="center" valign="center"><?php echo $user[4]; ?></td>
		<td align="center" valign="center"><?php 
			if ($user[5] == 1){
				echo 'Admin';	
			} 
			else{
				echo 'User';
			}
		 ?></td>
  	</tr>

	<?php
	}

	$del=$lev="";
    if(isset($_GET['del'])){
		$del=$_GET['del'];
	}
	
	if(isset($_GET['lev'])){
		$lev=$_GET['lev'];
	}
	
	if(!empty($del)){	
		$del_stmt = $db->prepare("DELETE FROM `UserAccount` WHERE account=?");
		$del_stmt->bind_param("s", $del);
		$del_stmt->execute();
			
		if($del_stmt){
			echo '<script>alert("Delete Succeeded")</script>';
    	}
		else{
			echo '<script>alert("Delete member Failed.")</script>';
		}
		echo '<script>window.location.href = "homeAdmin.php"</script>';
		//header('Location: homeAdmin.php');
	}

	if(!empty($lev)){	
		$lev_stmt = $db->prepare("UPDATE `UserAccount` SET isAdmin=1 WHERE account=?");
		$lev_stmt->bind_param("s", $lev);
		$lev_stmt->execute();
		
		if($lev_stmt){
			echo '<script>alert("Level up Succeeded")</script>';
    	}
		else{
			echo '<script>alert("Level up member Failed.")</script>';
		}
		echo '<script>window.location.href = "homeAdmin.php"</script>';
		//header('Location: homeAdmin.php');
	}

	?>
	
	</table>

</body>
</html>