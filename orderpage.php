<?php 
	session_start();
	include('ConnectToMySQL.php');

	if (!isset($_SESSION['USER'])) 
	{
		//echo '<meta http-equiv=REFRESH CONTENT=1;url=index.html>';
		echo '<script>window.location.href = "index.html"</script>';
    	exit;
   	} 

   	function text_input($data){
		$data = trim($data);
		$data = stripslashes($data);
		$data = htmlspecialchars($data);
		return $data;
	}
	$edit=0;
	if(isset($_GET['edit'])){
		$edit=text_input($_GET['edit']);
	}
 ?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>orderpage</title>
</head>
<body>
	<tr><td align="center" valign="center"><a href="home.php">Back to Home page.</a></td><br></tr>
	<td align="center" valign="center"><a href="fav.php">Favorite </a></td><br>
	<td align="center" valign="center"><a href="logout.php">Log Out </a></td><br>
	<h1>Here is Order page.</h1>

    	<?php 
		$user=$_SESSION['USER'];
		$sql=$db->prepare("SELECT * From House Inner join UserAccount On House.owner_id=UserAccount.id 
				inner join house_location on  house_location.house_id=House.id
				inner join Location on Location.id=house_location.id
                inner join `Order` on `Order`.house_id=House.id
                where user_id=?");
 		$sql->bind_param("s",$user);
		$sql->execute();
		$result= $sql->get_result();
		if (mysqli_num_rows($result) == 0) { 
   			echo "You don't have any ordered house.<br>"; 
		} 
		else {
			$id=15; $name=1; $price=2; $location=14; $time=3; $owner_name=6;
			$date = new DateTime('now'); $aftoday=new DateInterval('P1D'); $today=$date->add($aftoday); $today=$today->format('Y-m-d');
		 ?>

		<table style="width:100%">
 	 	<tr>
  		<th>ID</th>
  		<th>name</th>
  		<th>price</th>
  		<th>location</th>
  		<th>time</th>
  		<th>owner</th>
    	<th>information</th>
    	<th>checkin/checkout</th>
    	<th>option</th>
    	<th>option</th>
    	</tr>
		<?php 
			while($row=$result->fetch_row()){ 	
    		/*
    		if ($account==$row[$id]){
    			continue;
    		}
    		$account=$row[$id]; */
    	?>
    		<tr>
			<td align="center" valign="center"><?php echo $row[$id] ?></td>	<!--id-->
			<td align="center" valign="center"><?php echo $row[$name] ?></td>	<!--name-->
	    	<td align="center" valign="center"><?php echo $row[$price] ?></td>	<!--price-->
	    	<td align="center" valign="center"><?php 

	    		$loca_sql="SELECT name FROM `house_location` inner join Location on house_location.id=Location.id Where house_id='$row[0]'";
				$loca_result=$db->query($loca_sql);
				$loca_row=mysqli_fetch_row($loca_result);
				if(mysqli_num_rows($loca_result) == 0){
					echo "unknown";
				}
				else{
					echo $loca_row[0]."<br>"."<br>";
				}
    	 	?>
    	 	</td>	<!--locate-->
		    <td align="center" valign="center"><?php echo $row[$time] ?></td>	<!--time-->
		    <td align="center" valign="center"><?php echo $row[$owner_name] ?></td>	<!--owner-->
			<td align="center" valign="center"><?php 

				$info_sql="SELECT Information.name FROM Information 
						INNER JOIN house_Information On house_Information.id=Information.id 
						Where house_id='$row[$id]'";

				$info_result=$db->query($info_sql);

				while($info_row=mysqli_fetch_row($info_result)){
					echo $info_row[0]."<br>";
				}
				echo "<br>";

			?></td>
			
			<td align="center" valign="center">
				<form method="POST">
					<?php echo $row[18]."<br>".$row[19]."<br>"; ?>
					<?php if($edit!=0 && $row[$id]==$edit){
						?><input type="date" name="start" min="<?php echo $today ?>"><br><input type="date" name="end" min="<?php echo $today ?>">
					<?php } ?>
				
			</td>
			<?php if(!$edit){
				?><td align="center" valign="center"><a href="orderpage.php?edit=<?php echo $row[$id]; ?>&house=<?php echo $row[0]; ?>">Edit</td>
			<?php	
			}
			else if ($edit!=0){
				?><td align="center" valign="center"><input type="submit" name="OK"></td><?php 
			} ?>

			</form>
			<td align="center" valign="center"><a href="orderpage.php?del=<?php echo $row[$id]; ?>">Delete</a></td>
  			</tr>
		<?php } }

		$del="";
		if(isset($_GET['del'])){
			$del=text_input($_GET['del']);
		}
		if (!empty($del)){
			$del_stmt = $db->prepare("DELETE FROM `Order` WHERE house_id=? and user_id=$user");
				$del_stmt->bind_param("s", $del);
				$del_stmt->execute();


			if($del_stmt){
				echo '<script>alert("Delete Succeeded")</script>';
		    }
			else{
				echo '<script>alert("Delete Failed.")</script>';
			}
			echo '<script>window.location.href = "orderpage.php"</script>';	
		}

		if (isset($_POST['OK'])){
			$start=$_POST['start'];
			$end=$_POST['end'];
			$house=$_GET['house'];

			$check=$db->prepare("SELECT * from `Order` where id!=? and house_id=? and id in (select id from `Order` where begin<='$end' and end>='$start')");
			$check->bind_param("ii", $edit, $house);
			$check->execute();
			$res=$check->get_result();

			if (mysqli_num_rows($res) == 0){
				$up=$db->prepare("UPDATE `Order` SET begin='$start' , end='$end' WHERE `Order`.id=? and user_id=$user ");
				$up->bind_param("i", $edit);
				$up->execute();

				if($up){
					echo '<script>alert("UPdate Succeeded")</script>';
				}	
			}
			else{
					echo '<script>alert("update failed")</script>';	
			}
	
			echo '<script>window.location.href = "orderpage.php"</script>';
		}
	?>
</body>
</html>