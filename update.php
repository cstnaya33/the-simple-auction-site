<?php 
	session_start();
	include('ConnectToMySQL.php');
	include('info_arr.php');
?>
<htmL>
<head>
	<meta charset="UTF-8">
	<title>Update a house</title>
	<link type="text/css" rel="stylesheet" href="big_style.css">
</head>
<body>

	<div class="login_board">
		<br><br><h1>Update a House</h1>
		<form method="post">
			<p>
			id: <input type="text" name="id">
			</p>

			<p>
			name: <input type="text" name="name">
			</p>

			<p>
			price: <input type="text" name="price">
			</p>

			<p>
			location: <input type="text" name="location">
			</p>

			<p>
			info: <br><br>
				<?php 
				$info_sql="SELECT distinct * FROM `Information` group by name order by name";
				$info_result=$db->query($info_sql);
				while($info_row=mysqli_fetch_row($info_result)){
					?><input type="checkbox" name="info[]" value="<?php echo $info_row[0]; ?>"><?php echo $info_row[1]; ?><br><br><?php
				}	?> 
			</p>

			<input type="submit" name="submit">
		</form>
		<p><a href="house.php">Cancel</a></p>
	</div>



<?php 
	$flag=1;	$id=$name=$price=$location="";

	function text_input($data){
		$data = trim($data);
		$data = stripslashes($data);
		$data = htmlspecialchars($data);
		return $data;
	}

	if ( (!empty($_POST['submit'])) && (empty($_POST['id']))){	
			$flag=0;
			echo '<script type="text/javascript">alert("You need to fulfill id.");</script>'; 
		}

	/*******************************************************/

	if(!empty($_POST['name']) || !empty($_POST['price']) || !empty($_POST['location']) ){	
			$id=text_input($_POST['id']);
			$name = text_input($_POST['name']);
			$price = text_input($_POST['price']);
			$location = text_input($_POST['location']);

			if(!empty($name)){
				$sql=$db->prepare("SELECT name from House");
				$sql->execute();
				$result=$sql->get_result();
				while($row=$result->fetch_row()){
					if ($row[0] == $name){
						$flag=0;
						echo '<script type="text/javascript">alert("This name has been signed.");</script>';
						unset($_POST['name']);
						break;
					}
				}
				if ($flag){
					$namesql=$db->prepare("UPDATE House SET name=? WHERE id=?");
					$namesql->bind_param("ss", $name, $id);
					$namesql->execute();
					if ($namesql){
						echo '<script type="text/javascript">alert("Update Name Success");</script>';
					}
				}
			}

			if(!empty($price)){	
				if (!is_numeric($_POST['price'])){
					$flag=0;
					unset($_POST['price']);
					echo '<script>alert("Wrong price input format");</script>';
				}
				if ($flag){

					$psql=$db->prepare("UPDATE House SET price=? WHERE id=?");
					$psql->bind_param("ss", $price, $id);
					$psql->execute();
					if ($psql){
						echo '<script type="text/javascript">alert("Update Price Success");</script>';
					}					
				}			
			}

			if(!empty($location)){
				if ($flag){

					$find_loca = $db->prepare("SELECT name from Location where name=?");
					$find_loca->bind_param("s", $location);
					$find_loca->execute();
					$re_find=$find_loca->get_result();

					if (mysqli_num_rows($re_find)==0){
						echo "nnojciojieowjfsd"."<br>";
						$insert_loca = $db->prepare("INSERT INTO Location (name) VALUES ( ? )");
						$insert_loca->bind_param("s", $location);
						$insert_loca->execute();
						
					}

				
					$get_loca=$db->prepare("SELECT Location.id from Location where Location.name=?");
					$get_loca->bind_param("s",$location);
					$get_loca->execute();
					$re=$get_loca->get_result();
					$re=$re->fetch_all();
					foreach($re as $r){
						foreach($r as $row){
							$locasql=$db->prepare("UPDATE house_location SET id=? WHERE house_id=?");
							$locasql->bind_param("ss", $row, $id);
							$locasql->execute();
						}
					}
					
					if ($locasql){
						echo '<script type="text/javascript">alert("Update Location Success");</script>';
					}					
				}	
			}
	}

	/***********************************************************/

	$i="";
	if(!empty($_POST['info'])){
		$id=text_input($_POST['id']);
		$del_stmt = $db->prepare("DELETE FROM `house_Information` WHERE house_id=?");
				$del_stmt->bind_param("s", $id);
				$del_stmt->execute();

		$i=$_POST['info'];
		foreach ($i as $v){
			$new=$db->prepare("INSERT INTO `house_Information` (id, house_id) VALUES ('$v', ?)");
			$new->bind_param("s", $id);
			$new->execute();
		}
		echo '<script type="text/javascript">alert("Update Information Success");</script>';
	}

	if(!empty($_POST['submit']) && $flag){
		echo '<script>window.location.href="house.php"</script>';	
	}
	
?>