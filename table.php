<?php 
	session_start();
	include('ConnectToMySQL.php');

	if (!isset($_SESSION['USER'])) 
	{
		//echo '<meta http-equiv=REFRESH CONTENT=1;url=index.html>';
		echo '<script>window.location.href = "index.html"</script>';
    	exit;
   	} 

   	function text_input($data){
		$data = trim($data);
		$data = stripslashes($data);
		$data = htmlspecialchars($data);
		return $data;
	}


?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Table</title>
	<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
</head>
<body>
	<tr><td align="center" valign="center"><a href="home.php">Back to Home page.</a></td><br><br></tr>

	<table style="width:100%">
  	<tr>
  		<th>ID</th>
  		<th>Value</th>
  		<th>option</th>
  	</tr>
  	

  	<?php 

  	echo "Location<br><br>";
  	?>
  	Add Location:
  	<form method="post">
  		<input type="text" name="addlo">
  		<input type="submit" name="ok">
  	</form>

  	<?php 
  	$id=0; $name=1;
  	$lofo=$db->prepare("SELECT * from Location");
  	$lofo->execute();
	$lofo_re=$lofo->get_result();
	while($lo_row=$lofo_re->fetch_row()){
		?><tr>
		<td align="center" valign="center"><?php echo $lo_row[$id]; ?></td>
		<td align="center" valign="center"><?php echo $lo_row[$name]; ?></td>
		<td align="center" valign="center"><a href="table.php?del=<?php echo $lo_row[$id]; ?>">Delete</a></td></tr>

		 <?php
	}


	?></table><br> ***************************************************************************************************************************************************<br><br>Information<br><br> <table style="width:100%"> <?php

	?>
  	Add Infomation:
  	<form method="post">
  		<input type="text" name="addin">
  		<input type="submit" name="ok">
  	</form>

  	<?php

	$info=$db->prepare("SELECT * from Information");
	$info->execute();
	$info_re=$info->get_result();
	while($in_row=$info_re->fetch_row()){
		?><tr>
		<td align="center" valign="center"><?php echo $in_row[$id]; ?></td>
		<td align="center" valign="center"><?php echo $in_row[$name]; ?></td>
		<td align="center" valign="center"><a href="table.php?d=<?php echo $in_row[$id]; ?>">Delete</a></td></tr>

		 <?php
	}
	?> 

	</table> <?php

	$del="";
	if(isset($_GET['del'])){
		$del=text_input($_GET['del']);
	}
	if(!empty($del)){
		$d_lo=$db->prepare("DELETE FROM `Location` WHERE id=?");
		$d_lo->bind_param("s", $del);
		$d_lo->execute();
		if($d_lo){
			echo '<script>alert("Delete Succeeded")</script>';
		}
		else{
			echo '<script>alert("Delete FAILED")</script>';
		}
		echo '<script>window.location.href = "table.php"</script>';
	}

	$d="";
	if(isset($_GET['d'])){
		$d=text_input($_GET['d']);
	}
	if(!empty($d)){
		$d_in=$db->prepare("DELETE FROM `Information` WHERE id=?");
		$d_in->bind_param("s", $d);
		$d_in->execute();
		if($d_in){
			echo '<script>alert("Delete Succeeded")</script>';
		}
		else{
			echo '<script>alert("Delete FAILED")</script>';
		}
		echo '<script>window.location.href = "table.php"</script>';
	}
	$addlo="";
	if(!empty($_POST['ok']) && isset($_POST['addlo']) ){
		$addlo=text_input($_POST['addlo']);
	}
	if(!empty($addlo)){
		$add=$db->prepare("INSERT INTO Location (name) VALUES (?) ");
		$add->bind_param("s", $addlo);
		$add->execute();
		if($add){
			echo '<script>alert("insert Succeeded")</script>';
		}
		else{
			echo '<script>alert("insert FAILED")</script>';
		}
		echo '<script>window.location.href = "table.php"</script>';
	}

	$addin="";
	if(isset($_POST['addin'])){
		$addin=text_input($_POST['addin']);
	}
	if(!empty($addin)){
		$a=$db->prepare("INSERT INTO Information (name) VALUES (?) ");
		$a->bind_param("s", $addin);
		$a->execute();
		if($a){
			echo '<script>alert("insert Succeeded")</script>';
		}
		else{
			echo '<script>alert("insert FAILED")</script>';
		}
		echo '<script>window.location.href = "table.php"</script>';
	}
  	?>
</body>
</html>