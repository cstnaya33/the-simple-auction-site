- 可讓用戶註冊、登入、瀏覽使用者個人頁面的拍賣網站。
- 用戶可刊登、搜尋商品。
- 管理者擁有權限管理所有用戶帳號及商品資訊。

- 前端：html, css
- 後端：php , mysql
- 資料庫管理工具：phpMyAdmin (已失效)

-------

- index: 初始頁面
- register: 註冊
- ConnectToLogin: 後台登入操作
- home / homeAdmin: 主頁面 (帳戶資訊)
- table: 管理員監管全商品
- search: 搜尋功能
- orderpage: 預購商品(房子)
- update: 更新商品資訊頁
- fav: 用戶追蹤清單